package com.andropromise;

import java.util.List;

public class PromiseAll extends Promise {

    private List<Promise> promises = null;
    private int numOfDone = 0;
    public boolean isDone() {
        return numOfDone >= promises.size();
    }

    public PromiseAll(List<Promise> promises) {
        super("PromiseAll");
        printLog("PromiseAll created...");
        this.promises = promises;
        setAction(this::proceed);
    }

    private void proceed(IPromiseResult result) {
        for(Promise p : promises) {
            p.then(ret -> {
                printLog("PromiseAll.then : " + toString());
				updateStatus(result);
                return ret;
            }).thrown(exc -> {
                printLog("PromiseAll.onCatch : " + toString());
				updateStatus(result);
				return exc;
            }).start();
        }
    }

	private void updateStatus(IPromiseResult result) {
		numOfDone ++;
		printLog("PromiseAll.numOfDone : " + numOfDone + " of " + promises.size());
		if (isDone()) {
			setState(PromiseState.FULFILLED);
			Object[] datas = new Object[numOfDone];
			for (int i = 0; i < datas.length; i++) {
				datas[i] = promises.get(i).getData();
			}
			result.resolve(datas);
			numOfDone = 0;
		}
	}

    public void printLog(String log) { }

}
