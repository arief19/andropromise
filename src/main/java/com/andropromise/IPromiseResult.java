package com.andropromise;

public interface IPromiseResult {
    <T> void resolve(T result);
    void reject(Throwable exception);
	Object getData();
	//TODO 
    //<T> void reject(T result);
}
