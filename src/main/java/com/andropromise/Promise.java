/*
 *	Created by ♠ on 03/26/2021 13:34:04
 *	Copyright (c) 2021 . All rights reserved.
 */

package com.andropromise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Promise {

    private String name = "";
	private PromiseType type = PromiseType.SUCCESS;
    private PromiseState state = PromiseState.NONE;

    private IPromiseAction<IPromiseResult> action = null;
	private Object data = null;

    private Promise rootPromise = null; //root
    private List<Promise> nextPromiseList = new ArrayList<>();

    public Promise(String name) {
		this(name, null);
    }

    public Promise(IPromiseAction<IPromiseResult> action) {
        this("Promise", action);
    }

    public Promise(String name, IPromiseAction<IPromiseResult> action) {
        printLog("Promise \"" + name + "\" created...");
        this.name = name;
		rootPromise = this;
        setAction(action);
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

	public PromiseType getType() { return type; }
	public void setType(PromiseType type) { this.type = type; }

	public Object getData() { return data; }
	public void setData(Object data) { this.data = data; }

    public PromiseState getState() { return state; }
    protected void setState(PromiseState state) {
        printLog("[" + this + "].setState[prev : " + this.state + ", to : " + state + "]");
        this.state = state;
    }

    public Promise getRootPromise() { return rootPromise; }
    public void setRootPromise(Promise root) { this.rootPromise = root; }

    public void setAction(IPromiseAction<IPromiseResult> action) {
        this.action = action;
    }

	private Promise createPromise(final IFunc action, PromiseType type) {
		Promise promise = new Promise(getName() + "#", promiseResult -> {
			try { 
				Object result = action.invoke(data);
				printLog("promise.resolve[" + data + "] : " + result);
				promiseResult.resolve(result);
			} catch(Throwable exc) {
				exc.printStackTrace();
				printLog("promise.reject : " + exc);
				promiseResult.reject(exc);
			}
		});
		promise.setType(type);
		promise.setRootPromise(rootPromise);
		return promise;
	}

    public Promise then(final Promise promise) {
		printLog("then[" + promise + ", nextPromiseList : " + nextPromiseList.size() + "]");

		promise.setType(PromiseType.SUCCESS);
		promise.setRootPromise(rootPromise);
		nextPromiseList.add(promise);
		return promise;
	}

    public Promise then(final IFunc onSuccess) {
		printLog("then[" + onSuccess + ", nextPromiseList : " + nextPromiseList.size() + "]");

		Promise promise = createPromise(onSuccess, PromiseType.SUCCESS);
		nextPromiseList.add(promise);
		return promise;
	}

    public Promise onCatch(final IFailure onFailure) {
		return thrown(t -> {
			Exception exc = (t instanceof Exception) ? (Exception)t : new Exception("" + t);
			onFailure.invoke(exc);
			return t;
		});
	}

    public Promise thrown(final IFunc onFailure) {
		printLog("thrown[" + onFailure + ", nextPromiseList : " + nextPromiseList.size() + "]");

		Promise promise = createPromise(onFailure, PromiseType.FAILURE);
		nextPromiseList.add(promise);
		return promise;
	}

	synchronized void exec(PromiseType type) {
		if (type == this.type) {
			exec();
			return;
		}

		for (Promise nextPromise : nextPromiseList.toArray(new Promise[0])) {
			nextPromise.setData(data);
			nextPromise.exec(type);
		}
	}

	synchronized void exec() {
		printLog("exec[state : " + getState() + "] : " + data);
		if (this.action == null) return;
		
		for (Promise p : nextPromiseList.toArray(new Promise[0])) {
			p.setData(data);
		}

        setState(PromiseState.PENDING);

        this.action.run(new IPromiseResult() {
            @Override
            public <T> void resolve(T result) {
                printLog("[" + name + "].exec.resolve[result : " + result + "].[nextPromiseList : " + nextPromiseList.size() + "]");
				data = result;
                setState(PromiseState.FULFILLED);

				for (Promise p : nextPromiseList.toArray(new Promise[0])) {
					p.setData(result);
					p.exec(PromiseType.SUCCESS);
				}
            }

            @Override
            public void reject(Throwable exception) {
                printLog("[" + name + "].exec.reject[exception : " + exception + "].[nextPromiseList : " + nextPromiseList.size() + "]");
				data = exception;
                setState(PromiseState.REJECTED);
                exception.printStackTrace();

				for (Promise p : nextPromiseList.toArray(new Promise[0])) {
					p.setData(exception);
					p.exec(PromiseType.FAILURE);
				}
            }

			@Override
			public Object getData() { return data; }
        });
	}

    public void start() {
		Promise p = this;
        if(rootPromise != null) {
			p = rootPromise;
        }

        printLog("exec[" + p.getState() + "] \"" + name + "\" : " + action);
		if (p.getState() == PromiseState.PENDING) return; //already running
        p.exec();
    }

    @Override
    public String toString() {
        return "Promise[name : " + name + ", state : " + state + "]";
    }

	private void printLog(String log) { }

    public static Promise all(Promise[] promises) {
		return all(Arrays.asList(promises));
	}

    public static Promise all(List<Promise> promises) {
        return new PromiseAll(promises);
    }

    public interface IFunc {
        Object invoke(Object result) throws Throwable;
    }

	/*
	 * @deprecated : used to provide backward compatibility only
	 */
    public interface IFailure {
        void invoke(Exception exc) throws Throwable;
    }

    public enum PromiseState {
		NONE,
        PENDING,
        FULFILLED,
        REJECTED
    }

	public enum PromiseType {
		SUCCESS,
		FAILURE
	}

}
