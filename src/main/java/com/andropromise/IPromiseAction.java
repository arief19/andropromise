package com.andropromise;

public interface IPromiseAction<T> {
    void run(T param);
}
